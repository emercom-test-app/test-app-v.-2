const webpack = require('webpack');
const pkg = require('./package.json');

module.exports = {
  css: {
    loaderOptions: {
      css: {
        localIdentName: `${pkg.libname}[name]_[local]_[hash:base64:5]`,
      },
    },
  },
  lintOnSave: false,
  configureWebpack: () => {
    const customConfig = {
      plugins: [
        new webpack.ProvidePlugin({
          $: 'jquery',
          jquery: 'jquery',
          jQuery: 'jquery',
          'window.jquery': 'jquery',
          'window.jQuery': 'jquery',
          'window.$': 'jquery',
        }),
        new webpack.DefinePlugin({
          LIBNAME: JSON.stringify(pkg.libname),
        }),
      ],
    };
    if (process.env.VUE_CLI_BUILD_TARGET === 'lib') {
      customConfig.externals = [
        'bootbox',
        'bootstrap',
        'font-awesome',
        'jquery',
        'vue',
      ];
    }
    return customConfig;
  },
};
