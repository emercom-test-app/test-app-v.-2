## Установка

``` bash
# Все по стандарту
npm install

# Запуск в dev-режиме на localhost:8080
npm run dev

# Сборка
npm run build
```

## Использование компонента

Директива

```html
<DmitryOrloffSelect
  v-model="value"
  :options="options"
  :disabled="disabled"
  />
```

```в коде компонента
import DmitryOrloffSelect from 'DmitryOrloffSelect.vue';

export default {
  components: {
    DmitryOrloffSelect,
  },
  data: () => ({
    options: [
      { value: 1, text: 'Первый' },
      { value: 2, text: 'Второй' },
    ],
    value: null,
    secondValue: '1',
    disabled: false,
  }),
};
```

## Пропсы

Prop | Тип | Значение по умолчанию | Описание
-------|------|---------|-------------
`v-model` | any | `null` | Через `v-model` передаем компоненту значения
`:options` | Array | `[]` | Параметры для отображения. Передаются как объекты в массиве или просто в виде option в HTML шаблона компонента
`:search` | Boolean | `false` | Показывать/скрывать поле поиска
`:multiple` | Boolean | `false` | Одиночны или множественный поиск
`:controls` | Boolean | `false` | Кнопки выбора и отмены выбора всех вариантов
`:collapseHeaders` | Boolean | `false` | Показать/скрыть ссылки на заголовки элементов
`:displayMax` | Number | `0` | Количество выбранных элементов, после которого из списка они превращаются в надпись "Выбрано: N"."0" - все элементы
`:displayText` | String | `"Выбрано: {0}"` | Текст, отображаемы при достижении `displayMax`. "{0}" выведет общее их число

