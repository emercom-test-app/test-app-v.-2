import Vue from 'vue';
import DmitryOrloffSelect from './DmitryOrloffSelect.vue';

const components = {
  DmitryOrloffSelect,
};

// global register components
function register() {
  Object.keys(components).forEach(name => Vue.component(
    `${LIBNAME}${name}`,
    components[name],
    {
      name: `${LIBNAME}${name}`,
    }
  ));
}

export {
  DmitryOrloffSelect,
};

export default register;
